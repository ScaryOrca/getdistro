# GetDistro.co
GetDistro.co is a quick and easy way to download any Linux distribution.

## Usage
Fetch the latest release of a distribution:
```shell
$ wget getdistro.co/fedora -O fedora.iso 
```

Fetch the latest Ubuntu LTS:
```shell
$ wget getdistro.co/ubuntu/lts -O ubuntu.iso
```

Fetch openSUSE Tumbleweed network image:
```shell
$ wget getdistro.co/opensuse/tumbleweed/net -O opensuse.iso
```

Fetch the KDE spin of a distribution:
```shell
$ wget getdistro.co/fedora/kde -O fedora.iso
```

